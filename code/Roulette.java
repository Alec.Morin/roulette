import java.util.Scanner;
public class Roulette{

    public static void main(String[]args){
        Scanner scan = new Scanner(System.in);
        RouletteWheel wheel = new RouletteWheel();
        int startMoney= 1000;
        int currentMoney = startMoney;
        boolean betting=false;

        System.out.println("Would you like to bet?");
        String answer = scan.next();

        if (answer.equals("yes")){
            betting=true;
        }
        while(betting){
            boolean validBet=false;
            System.out.println("Which number would you like to bet on?: ");
            int numBetOn = scan.nextInt();
            System.out.println("How much money would you like to bet: ");
            int betAmount = scan.nextInt();

            while(!validBet){
                if (betAmount > currentMoney){
                    System.out.println("You don't have this much money. You currently have "+ currentMoney + "$\nPlease enter a new number: ");
                    betAmount = scan.nextInt(); 
                }
                else{
                    validBet=true;
                }
            }

            wheel.spin();
            if (numBetOn == wheel.getValue()){
                System.out.println("You won: "+ winning(betAmount, wheel.getValue()));
                currentMoney = winning(betAmount, wheel.getValue()) + currentMoney;
                System.out.println("You now pocess "+ currentMoney);
            }
            else{
                currentMoney = currentMoney-betAmount;
                System.out.println("Unfortunately, you didn't get the right number\nThe roulette number landed on "+ wheel.getValue());
            }

            if (broke(currentMoney)){
                betting=false;
                System.out.println("It looks like you ran out of money :(\nSee you next time!");
            }
            else{
                System.out.println("Would you like to continue playing?");
                answer = scan.next();
                if (answer.equals("no")){
                    betting = false;

                    if (currentMoney > startMoney){
                        System.out.println("Congratulations, you won "+ (currentMoney-startMoney) +"$!");
                    }
                    else{
                        System.out.println("Unfortunately, you lost "+ (currentMoney-startMoney) +"$");
                    }
                }
            }
        }
    }

    public static int winning(int moneyBet, int target){
        return moneyBet * (target-1);
    }

    public static boolean broke(int currentAmount){
        if (currentAmount <= 0){
            return true;
        }
        return false;
    }
}