import java.util.Random;
public class RouletteWheel {
    
    private Random numGenerator;
    private int number;

    public RouletteWheel(){
        this.numGenerator = new Random();
        this.number = 0;
    }

    public void spin(){
        this.number = this.numGenerator.nextInt(37);
    }

    public int getValue(){
        return this.number;
    }
}
